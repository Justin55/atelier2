<?php

require_once 'vendor/autoload.php';

# <------ USE ------> #
use Slim\Slim as Slim;
use gestionnaire\api\view\Rules as Rules;


#### DISPATCHER ####

$app = new Slim();
	// Route NOT FOUND
$app->notFound(function () use ($app) {
	$c = AbstractController::displayError('404', ["codeError" => "404","errorMessage" => "La ressource demande n'existe pas"]);
});
	// Route GET
$app->get('/', function() {
	$v = Rules::displayRules();
});

$app->run();