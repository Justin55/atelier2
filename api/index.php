<?php

require_once '../vendor/autoload.php';

# <------ USE ------> #
use Slim\Slim as Slim;
use utils\Connect;
use utils\checkToken as CToken;
use gestionnaire\api\view\Rules as Rules;
use gestionnaire\api\controller\AbstractController as AbstractController;
use gestionnaire\api\controller\serieController as SController;
use gestionnaire\api\controller\photosController as PController;
use gestionnaire\api\controller\partieController as PaController;


	// Méthode de connexion

Connect::EloConfigure('../../conf/config.ini');

#### DISPATCHER ####

$app = new Slim();
	// Route NOT FOUND
$app->notFound(function () use ($app) {
	$c = AbstractController::displayError('404', ["codeError" => "404","errorMessage" => "La ressource demande n'existe pas"]);
});
	// Route GET
$app->get('/', function() {
	$v = Rules::displayRules();
});

$app->get('/play/series','check_token', function() {
	$SC = SController::getSeries();
});

$app->get('/play/series/:id','check_token', function($id) {
	$SC = SController::getSeriebyID($id);
})->name('serie2id');

$app->get('/play/series/:id/photos','check_token', function($id) {
	$PC = PController::getPhotosBySerie($id);
})->name('serie2photos');

$app->get('/play/photos','check_token', function(){
	$PC = PController::getPhotos();
});

$app->get('/play/photos/:id','check_token', function($id) {
	$PC = PController::getPhotosbyID($id);
})->name('photos2id');

$app->get('/play/highscore' , function() {
	$PaC = PaController::getHighScore();
});
	// Route POST
$app->post('/play/create' , function() {
	$token = create_token();
	$PaC = PaController::postCreateGame($token);
});
	// Route PUT
$app->put('/play/start','check_token', function() {
	$PaC = PaController::putStartGame();
});

$app->put('/play/end','check_token', function() {
	$PaC = PaController::putEndGame();
});

$app->put('/play/player/:player','check_token', function($player) {
	$PaC = PaController::putPlayer($player);
});

	// Méthode de check TOKEN
function check_token() {
	$app = Slim::getInstance();
	$k = $app->request->get('token');
	$r = CToken::check($k);
};

	// Méthode de génération de TOKEN
function create_token() {
	$factory = new RandomLib\Factory;
	$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
	$token = $generator->generateString(32,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/_-');
	return $token;
}

$app->run();