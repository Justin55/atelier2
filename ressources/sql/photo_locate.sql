-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Ven 05 Février 2016 à 11:19
-- Version du serveur :  5.5.34
-- Version de PHP :  5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `photo_locate`
--

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE `partie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `nb_photos` int(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `score` int(100) NOT NULL,
  `joueur` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_serie` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=103 ;

--
-- Contenu de la table `partie`
--

INSERT INTO `partie` (`id`, `token`, `nb_photos`, `status`, `score`, `joueur`, `id_serie`) VALUES
(1, 'ktkBBpQazTHwO7PGHwFhYR7aUn/pJ/Ob', 10, 3, 180, 'Justin', '3'),
(2, 'jc9YdYKfPDxrQkkckGDxzvfWPd/g0d_T', 10, 3, 160, 'Justin', '3'),
(3, 'ZQTt522dBDiBs9uf4z2hB7g_bCt3YlpY', 10, 2, 0, 'Hasan', '3');

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE `photo` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `description` varchar(700) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'lat?lon',
  `url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `id_serie` int(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Contenu de la table `photo`
--

INSERT INTO `photo` (`id`, `description`, `position`, `url`, `id_serie`) VALUES
(1, 'IUT Nancy Charlemagne', '48.6828;6.1611', 'iut.jpg', 3),
(2, 'Well & Fit', '48.6797;6.1560', 'salle.jpg', 3),
(3, 'Chez Fred', '48.6830;6.1687', 'fred.jpg', 3),
(4, 'Chez Hasan', '48.6731;6.1598', 'haso.jpg', 3),
(5, 'La Poste de Tomblaine', '48.6862;6.2098', 'poste.jpg', 3),
(6, 'Un petit McDo', '48.6800;6.1981', 'mcdo.jpg', 3),
(7, 'École des Mines', '48.6723;6.1633', 'mines.jpg', 3),
(8, 'Place Stanislas', '48.6870;6.1730', 'stan.jpg', 3),
(9, 'Chez Justin', '48.6836;6.1587', 'justin.jpg', 3),
(10, 'CHU Nancy', '48.6850;6.1893', 'chu.jpg', 3);

-- --------------------------------------------------------

--
-- Structure de la table `serie`
--

CREATE TABLE `serie` (
  `id` int(150) NOT NULL AUTO_INCREMENT,
  `ville` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `map_refs` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'lat?lon?zoom',
  `distance` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `serie`
--

INSERT INTO `serie` (`id`, `ville`, `map_refs`, `distance`) VALUES
(1, 'Test', '30.55;6.53;10', 200),
(3, 'Nancy', '48.68;6.18;13', 300),
(4, 'Verdun', '10.789;10.871;12', 300);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `user`, `pass`) VALUES
(1, 'test', '$2y$13$ZM/hvqKUIMGCc3QxH3.5B.9ez/Cd9zPsBdegytUv8EBQAQpLoxjoW');
