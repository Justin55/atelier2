angular.module('photoLocate').directive('map', ['Map',
	function(Map) {
	  return {
	    templateUrl: '../ressources/templates/map.html',
	    link: function(scope, element, attrs) {
	      	var mapDisplay = Map(48.684, 6.185, 13);
	      	scope.add = function(){
	      		mapDisplay.addMark();
	      	}
	      }
	    }
 }]);