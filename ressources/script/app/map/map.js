angular.module('photoLocate').service('Map', ['$http', 
  function($http) {
    var Map = function(lat, lon, zoom){
    	var map = L.map('map', { zoomControl:false }).setView([lat, lon], zoom);
    	map.touchZoom.disable();
		map.doubleClickZoom.disable();
		map.scrollWheelZoom.disable();
		map.boxZoom.disable();
		map.keyboard.disable();
		map.dragging.disable();
    	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
    }

    Map.prototype.addMark = function() {
    	console.log('addMark');
    };


    return Map;
  
}]);
