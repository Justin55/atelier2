angular.module('photoLocate').controller('GameController', ['$scope', '$http', '$rootScope', '$interval', '$timeout',
  function($scope, $http, $rootScope, $interval, $timeout) {

  	/******** NEW START ***********/

  	// Declaration des variables du scope
  	$scope.token = "";
  	$scope.listPhotos = [];
  	$scope.count = 0;
  	$scope.id = 0;
  	$scope.position = "";
  	$scope.score = 0;
  	$scope.scoreTotal = 0;
  	$scope.distance = 0;
  	$scope.timer = 10;
  	$scope.timerDisplay = $scope.timer;
  	$scope.pseudo = "guest";
  	$scope.highScore = 0;

  	// Variables de flag
  	$scope.start = true;
  	$scope.launchNextImg = false;
  	$scope.timerRun = false;
  	$scope.timerStop;

    // Déclaration des variables du rootScope


    // Déclaration des méthodes
  	$scope.createGame = function(){

  		var data = {
  			joueur : $scope.pseudo,
  			id_serie  : "3"
  		};

  		$scope.id = data.id_serie;

  		$http.post('../api/play/create', data).then(function(response){
  			$scope.token = response.data;
  			$scope.initGame(data.id_serie);
  		}, function(error){
  			console.log(error);
  		});
  	}

  	$scope.initGame = function(id){
  		$http.get('../api/play/series/'+id+'?token='+$scope.token).then(
  			function(response){
  				$scope.distance = response.data.serie.distance;
  				$scope.initMap(response.data.serie.map_refs);
  				$scope.getImages(id);
  				$scope.toggleNext();
  			}, function(error){
  				console.log(error);
  			});
  	}

  	$scope.initMap = function(data){
  		coord = data.split(';');
  		$scope.map = L.map('map', { zoomControl:false }).setView([coord[0], coord[1]], coord[2]);
  		$scope.tile_layer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {}).addTo($scope.map);
  		$scope.map.touchZoom.disable();
		$scope.map.doubleClickZoom.disable();
		$scope.map.scrollWheelZoom.disable();
		$scope.map.boxZoom.disable();
		$scope.map.keyboard.disable();
		$scope.map.dragging.disable();
		$('.leaflet-container').css('cursor','crosshair');
  	}

  	$scope.clearMap = function(){
  		if ($scope.marker) {
		  	$scope.map.removeLayer($scope.marker);
		  	$scope.map.removeLayer($scope.markerTrue);
		}
  	}

  	$scope.getImages = function(id){
  		$http.get('../api/play/series/'+id+'/photos'+'?token='+$scope.token).then(
  			function(response){
  				$scope.listPhotos = response.data.Photos;
  				$scope.blend();
  			}, function(error){
  				console.log(error);
  			});
  	}

  	$scope.displayImage = function(){

  		$scope.description = $scope.listPhotos[$scope.count].photo.description;
  		$scope.position = $scope.listPhotos[$scope.count].photo.position;
  		$scope.url = $scope.listPhotos[$scope.count].photo.url;
  		
  		$scope.photoInfo = {'description' : $scope.description, 
  							'url' : '../ressources/img/'+$scope.url};

  		document.getElementById('img').setAttribute('src', $scope.photoInfo.url);
  		document.getElementById('title').textContent = $scope.photoInfo.description;
  		document.getElementById('score').textContent = $scope.scoreTotal;
  		$scope.addCount();
  		// console.log($scope.photoInfo);
  		
  	}

  	$scope.addCount = function()
  	{
  		if ($scope.count < 10) {
  			$scope.count++;
  			console.log($scope.count);
  		}  		
  	}

  	$scope.addMark = function(){
  		$scope.displayImage();
  		$scope.map.on('click', function(e) {
  			$scope.stopTimer();
  			var coord = $scope.position.split(';');
  			var position = new L.LatLng(coord[0], coord[1]);
  			$scope.clearMap();
		  	$scope.marker = new L.marker(e.latlng).addTo($scope.map);
		  	
		  	$timeout(function(){
		  		$scope.markerTrue = new L.marker(position).addTo($scope.map).bindPopup($scope.description+' est ici').openPopup();
		  	}, 1000);
		  	var distance = position.distanceTo(e.latlng).toFixed(0);
		  	$scope.calcScore(distance);

		  	$scope.resetTimer();
  			
  			$timeout(function(){
  				$scope.toggleNext();
  			}, 1500);
  		});
  	}

  	$scope.calcScore = function(distance){
  		if ($scope.count < 10) {
  			$scope.score = 0;
  			if (distance) {
  				distance = parseInt(distance);
			    if (distance <= $scope.distance) {
			    	$scope.score = 5;
			    	// console.log('Score pour l\'image : 5');
			    }
			    else if ((distance > $scope.distance) && (distance <= 2 * $scope.distance)) {
			    	$scope.score = 3;
			    	// console.log('Score pour l\'image : 3');
			    }
			    else if ((distance > 2 * $scope.distance) && (distance <= 3 * $scope.distance)) {
			    	$scope.score = 1;
			    	// console.log('Score pour l\'image : 1');
			    }
			    else if (distance > 3 * $scope.distance) {
			    	$scope.score = 0;
			    	// console.log('Score pour l\'image : 0');
			    }
  			}
  			if ($scope.timer) {
  				var timer = parseInt($scope.timer);
  				timer = 10 - timer;
  				if (timer <= 2) {
  					$scope.score *= 4;
  				}
  				else if ((timer > 2) && (timer <= 5)) {
  					$scope.score *= 2;
  				}
  				else if ((timer > 5) && (timer <= 9)) {
  					$scope.score *= 1;
  				}
  			}
			$scope.scoreTotal += $scope.score; 
			$('#scoreImg').addClass('scoreTotal').text("Score pour cette image : "+$scope.score);  
		}
  	}

  	$scope.launchGame = function(){
  		$scope.updateUser();
  		$scope.toggleStart();
  		$scope.startTimer();
  		$scope.addMark();
  		$("div.displayInfo").removeClass('masked');
  	}

  	$scope.nextImg = function(){
  		$scope.toggleNext();
  		$scope.displayImage();
  		// $scope.addCount();
  		$scope.startTimer();
  		$("div.displayInfo").removeClass('masked');
  	}

  	$scope.toggleStart = function(){
  		$scope.start = false;
  		$scope.inGame();
  	}

  	$scope.toggleNext = function(){
  		if ($scope.count < 10) {
  			$('#next > div > div > div > button').text("Image suivante");
	  		$scope.clearMap();
	  		if ($scope.launchNextImg == false) {
	  			$scope.launchNextImg = true;
	  			$("#next").hide();
	  		}
	  		else {
	  			$scope.launchNextImg = false;
				$("#next").show();
				$("div.displayInfo").addClass('masked');
	  		}
  		}
  		else {
  			$scope.launchNextImg = false;
			$("#next").show();
			$("div.displayInfo").addClass('masked');
  			$scope.endGame();
  		}
  		
  	}

  	$scope.endGame = function(){
  		$("#next > div.load").remove();

  		var div = $('<div>').addClass('display');
  		var firstDiv = $('<div>');
  		var secondDiv = $('<div>');
  		var thirdDiv = $('<div>');
  		
  		var myT = $("<h3>").addClass('scoreTotal').text('Score : '+$scope.scoreTotal);
  		var myB = $("<button>").addClass('btnCustom').attr({
  			'onclick': 'location.reload()',
  			'id': 'relaunch'
  		}).text('Nouvelle partie');
  		
  		$(secondDiv).append(myT);
  		$(thirdDiv).append(myB);

  		$(firstDiv).append(secondDiv);
  		$(firstDiv).append(thirdDiv);

  		$(div).append(firstDiv);

  		$("#next").append(div);
  		
  		$scope.closeGame();
  	}

  	$scope.blend = function(){
  		for (var position = $scope.listPhotos.length - 1; position >= 1; position--) {
  			var random = Math.floor(Math.random()*(position + 1));
  			var save = $scope.listPhotos[position];
  			$scope.listPhotos[position] = $scope.listPhotos[random];
  			$scope.listPhotos[random] = save;
  		}
  	}

  	$scope.startTimer = function() {
  		$scope.resetTimer();
  		$scope.timerDisplay = 10;
        if (angular.isDefined($scope.timerStop)) return;

        $scope.timerStop = $interval(function() {
            if ($scope.timer > 0) {
              $scope.timer--;
              if ($scope.timer < 10) {
              	$scope.timerDisplay = "0"+$scope.timer;
              }
              
            } else {
              $scope.stopTimer();
              $scope.toggleNext();
            }
        }, 1000);
    }

    $scope.stopTimer = function() {
        if (angular.isDefined($scope.timerStop)) {
           $interval.cancel($scope.timerStop);
           $scope.timerStop = undefined;
        }
    }

    $scope.resetTimer = function(){
    	$scope.timer = 10;
    	$scope.displayTimer = 10;
    }

    $scope.updateUser = function(){
    	var pseudo = $("#pseudo").val();
    	$scope.pseudo = pseudo;
    	$http.put('../api/play/player/'+pseudo+'?token='+$scope.token).then(
  			function(response){
  				console.log(response.data);
  			}, function(error){
  				console.log(error);
  		});
    }

    $scope.inGame = function(){
    	$http.put('../api/play/start?token='+$scope.token).then(
  			function(response){
  				console.log(response.data);
  			}, function(error){
  				console.log(error);
  		});
    }

    $scope.closeGame = function(){
    	var score = {'score': $scope.scoreTotal};
    	console.log(score);
    	$http.put('../api/play/end?token='+$scope.token, score).then(
  			function(response){
  				console.log(response.data);
  			}, function(error){
  				console.log(error);
  		});
    }

    $scope.getScore = function(){
    	$('#displayScore').children().remove();
    	$http.get('../api/play/highscore?serie=3').then(
  			function(response){
  				$scope.highscore = response.data;
          console.log(response.data);

  			var myD = $('<div>').addClass('modal').attr('id', 'modal1');
  			var firstD = $('<div>').addClass('modal-content');
  			var secondD = $('<div>').addClass('modal-footer');
  			var title = $('<h4>').text('Meilleurs Scores :');
  			var button = $('<button>')
  							.attr('id', 'btnCloseScore')
  							.addClass('btn')
  							.addClass('orange')
  							.text('Fermer');
  			var list = $('<ul>').addClass('collection');

  			$.each($scope.highscore, function(key, val) {
  				var myLi = $('<li>').addClass('collection-item active').addClass('orange')
  				.append(val.joueur+" : "+val.score);
  				list.append(myLi);
  			});
  			
  			firstD.append(title);
  			firstD.append(list);
  			secondD.append(button);

  			myD.append(firstD);
  			myD.append(secondD);

  			myD.attr({
  				'position': 'absolute',
  				'z-index': '9999999999999'
  			});


  			$('#displayScore').append(myD);
  			$('#displayScore').children().show();

  			$("#btnCloseScore").click(function() {
  				$('#displayScore').children().hide();
  			});

  			}, function(error){
  				console.log(error);
  			});
    }


  	/*********** END **************/


  	// Début programme
  	
  	$scope.createGame();
  	
  	
  	
  	// Fin programme

}]);
