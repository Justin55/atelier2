<?php 

namespace utils;

use Illuminate\Database\Eloquent\Model as DB;
use Slim\Slim as Slim;

/**
* 
*/
class checkToken extends DB
{
	protected $table='partie';
	protected $primaryKey='id';
	public $timestamps=false;

	private static function displayError($code, $message)
	{
		$app = Slim::getInstance();

		$app->response->headers->set('Content-Type', 'application/json');
		$app->response->setStatus($code);

		echo json_encode($message);	
	}

	public static function check($key){
		$r = checkToken::where('token', $key)->get();
		if ($r->toArray()) {
			$tmp = $r[0]->id;
			$k = checkToken::find($tmp);
		}
		else if(!$r->toArray()) {
			$app = Slim::getInstance();
			self::displayError("401",array ("codeError" => "401","errorMessage" => "you must be authentificated to view this ressource","message"=>"Token invalid"));
			$app->stop();
		}
	}	

}