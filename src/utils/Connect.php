<?php

// Namespace
namespace utils;
// Use
use Illuminate\Database\Capsule\Manager as DB;

class Connect
{
	function __construct()
	{
		
	}

	public static function EloConfigure($filename)
	{
		$config= parse_ini_file($filename);

		if (!$config) throw new Exception("App::eloConfigure: could not parse config file $filename<br/>");

		$capsule = new DB();
		$capsule->addConnection($config);
		$capsule->setAsGlobal();
		$capsule->bootEloquent();
	}	

}
