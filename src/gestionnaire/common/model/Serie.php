<?php

#### Namespace ####
namespace gestionnaire\common\model;

#### USE ####
use Illuminate\Database\Eloquent\Model as DB;

/**
* 
*/
class Serie extends DB
{
	protected $table='serie';
	protected $primaryKey='id';
	public $timestamps=false;

	static public function findAll() {
		$serie = Serie::all();
		return $serie;
	}

	static public function findOne($id) {
		$serie = Serie::where('id',$id)->first();
		return $serie;
	} 

	public function photos() {
		return $this->hasMany('\gestionnaire\common\model\Photos','id_serie')->get();
	}

	public static function postSerie($t) {
		$serie = new Serie();

		foreach ($t as $key => $value) {
			$serie->$key = $value;
		}
		
		$serie->save();
	}

}