<?php

#### Namespace ####
namespace gestionnaire\common\model;

#### USE ####
use Illuminate\Database\Eloquent\Model as DB;

/**
* 
*/
class Photos extends DB
{
	protected $table='photo';
	protected $primaryKey='id';
	public $timestamps=false;

	public static function postPhoto($t) {
		$p = new Photos();

		foreach ($t as $key => $value) {
			$p->$key = $value;
		}
		$p->save();
	}

	public static function postPhotoSerie(){

	}

	static public function findOne($id) {
		$p = Photos::where('id',$id)->first();
		return $p;
	}

	static public function findAll() {
		$p = Photos::all();
		return $p;
	}
}