<?php

#### Namespace ####
namespace gestionnaire\common\model;

#### USE ####
use Illuminate\Database\Eloquent\Model as DB;

/**
* 
*/
class ClassName extends DB
{
	protected $table='A_modifier';
	protected $primaryKey='A_modifier';
	public $timestamps=false;

	function __construct(argument)
	{
		# code...
	}

}