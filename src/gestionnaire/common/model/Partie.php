<?php

#### Namespace ####
namespace gestionnaire\common\model;

#### USE ####
use Illuminate\Database\Eloquent\Model as DB;
use gestionnaire\api\controller\AbstractController as AbstractController;
use Slim\Slim as Slim;

/**
* 
*/
class Partie extends DB
{
	protected $table='partie';
	protected $primaryKey='id';
	public $timestamps=false;

	static public function findByToken($token) {
		$p = Partie::where('token',$token)->first();
		return $p;
	}

	static public function HighScore($serie) {
		$partie = Partie::where('id_serie',$serie)->take(10)->orderBy('score','desc')->get();
		return $partie;
	}

	static public function createGame($data) {
		$app = Slim::getInstance();

		$app->response->setStatus('201');

		$partie = new Partie();

		foreach ($data as $key => $value) {
			$partie->$key = $value;
		}

		$partie->status = "1";
		$partie->save();
	}

	static public function startGame($token) {
		$partie = Partie::findByToken($token);

		$partie->status = "2";
		$partie->save();
	}

	static public function endGame($token,$data) {
		$partie = Partie::findByToken($token);

		if ($partie->status=="3") {
			$a = AbstractController::displayError('400',array("codeError" => "400","errorMessage" => "Can't modify the score of a finished game"));
		}
		else {
			$partie->status = "3";
			$partie->score = $data->score;
			$partie->save();			
		}

	}
}