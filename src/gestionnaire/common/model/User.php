<?php

#### Namespace ####
namespace gestionnaire\common\model;

#### USE ####
use Illuminate\Database\Eloquent\Model as DB;

/**
* 
*/
class User extends DB
{
	protected $table='utilisateur';
	protected $primaryKey='id';
	public $timestamps=false;

	static public function findAll() {
		$u = User::all();
		return $u;
	}

	static public function findOne($id) {
		$u = User::where('id',$id)->first();
		return $u;
	} 

	static public function findHash($user) {
		$u = User::where('user', $user)->select('pass')->first();
		return $u;
	}
	public static function postUser($t) {
		$u = new User();

		foreach ($t as $key => $value) {
			$u->$key = $value;
		}
		$u->save();
	}
}