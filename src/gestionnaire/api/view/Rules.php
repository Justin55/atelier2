<?php 

namespace gestionnaire\api\view;

use Slim\Slim as Slim;
/**
* 
*/
class Rules 
{

	static public function displayRules() {
		$html = "
<!DOCTYPE html>
<html lang=\"fr\">
<head>
	<meta charset=\"UTF-8\">
	<title>PhotoLocate | Règles</title>
	<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
	<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css\">
	<script src=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js\"></script>
</head>

<body>

<header>
	<nav class='top-nav'>
		<div class='container'>
			<div class='nav-wrapper'>
				<a class='page-title'> RÈGLES DU JEUX </a>
			</div>
		</div>
	</nav>	
</header>

<div class='container'>
	<ul class='collection'>
		<li class='collection-item active'>Règles</li>
		<li class='collection-item'>Une partie consiste en une séquence de 10 photos à placer sur la carte d'une ville</li>
		<li class='collection-item'>Toutes les photos d'une série concernent la même ville et la même carte</li>
		<li class='collection-item'>Chaque réponse permet de gagner un certain nombre de points, en fonction de la précision du placement et de la rapidité pour répondre<li>
		<li class='collection-item'>L'objectif pour une partie est d'obtenir le maximum de points</li>
		<li class='collection-item'>La partie est terminée lorque les 10 photos ont été positionnées</li>
	</ul>
</div>

<footer class='page-footer'>
	<div class='footer-copyright'>
		<div class='container'>
			2015-2016 PhotoLocate
			<a class=\"btn right\" href=\"play/\">Game</a>
			<a class=\"btn right\" href=\"admin/\">Admin</a>
		</div>
	</div>
</footer>

</body>

</html>
		";

		echo $html;
	}

}