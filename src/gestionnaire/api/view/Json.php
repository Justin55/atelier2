<?php 

namespace gestionnaire\api\view;
use Slim\Slim as Slim;
/**
* 
*/
class Json 
{

###### TRANSLATE Series/Serie  ==>  JSON ######
	static public function TranslateSerieToJson($s,$id) {
		$app = Slim::getInstance();
		$app->response->headers->set('Content-Type', 'application/json');	
		
		$serie = $s->toArray();
		$serie['type']='Serie'; 

		$res = [
			'serie' => $serie
		];

		echo json_encode($res);		
	}

	static public function TranslateSeriesToJson($s) {
		$app = Slim::getInstance();
		$app->response->headers->set('Content-Type', 'application/json');

		$serie = [];

		foreach ($s as $key => $value) {
			$value = (object) $value;
			$link = $app->urlFor('serie2id', ['id' => $value->id]);
			$href = ['href' => $link];
			$links = ['self'=> $href];
			$temp2 = ['serie' => $value, 'links' => $links];
			array_push($serie, $temp2);
		}

		$series = ['Series' => $serie];

		echo json_encode($series);
	}
###### TRANSLATE Photos/Photo  ==>  JSON ######
	static public function TranslatePhotoToJson($p,$id) {
		$app = Slim::getInstance();
		$app->response->headers->set('Content-Type', 'application/json');	
		
		$photo = $p->toArray();
		$photo['type']='Photo'; 

		$res = [
			'photo' => $photo
		];

		echo json_encode($res);		
	}

	static public function TranslatePhotosToJson($p) {
		$app = Slim::getInstance();
		$app->response->headers->set('Content-Type', 'application/json');

		$photo = [];

		foreach ($p as $key => $value) {

			$value = (object) $value;
			$link = $app->urlFor('photos2id', ['id' => $value->id]);
			$href = ['href' => $link];
			$links = ['self'=> $href];
			$temp2 = ['photo' => $value, 'links' => $links];
			array_push($photo, $temp2);
		}

		$photos = ['Photos' => $photo];

		echo json_encode($photos);		
	}

	static public function TranslatePhotosBySeriesToJson($p) {
		$app = Slim::getInstance();
		$app->response->headers->set('Content-Type', 'application/json');

		$photo = [];

		foreach ($p as $key => $value) {

			$value = (object)$value;
			$link = $app->urlFor('photos2id', ['id' => $value->id]);
			$href = ['href' => $link];
			$links = ['self'=> $href];
			$temp2 = ['photo' => $value, 'links' => $links];
			array_push($photo, $temp2);
		}

		$photos = ['Photos' => $photo];

		echo json_encode($photos);		
	}
###### TRANSLATE HighScore  ==>  JSON ######

	static public function TranslateHGtoJson($s) {
		$app = Slim::getInstance();
		$app->response->headers->set('Content-Type','application/json');


		echo json_encode($s);
	}
}