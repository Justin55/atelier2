<?php 

namespace gestionnaire\api\controller;
/**
* 
*/
use Slim\Slim as Slim;

class AbstractController
{
	public static function displayError($code, $message)
	{
		$app = Slim::getInstance();

		$app->response->headers->set('Content-Type', 'application/json');
		$app->response->setStatus($code);

		echo json_encode($message);
	}
}