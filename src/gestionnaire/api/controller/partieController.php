<?php 

namespace gestionnaire\api\controller;

use gestionnaire\common\model\Partie as Partie;
use gestionnaire\api\view\Json as JSON;
use utils\createToken as CrToken;
use Slim\Slim as Slim;
/**
* 
*/


class partieController extends AbstractController
{	

	static public function getHighScore() {
		$app = Slim::getInstance();

		$app->response->headers->set('Content-Type', 'application/json');

		$serie = $app->request->get('serie');

		$partie = Partie::HighScore($serie);

		$v = JSON::TranslateHGtoJson($partie);
	}

	static public function postCreateGame($token) {
		$app = Slim::getInstance();

		$dataGet = json_decode($app->request->getBody());

		$app->response->setBody($token);

		$dataSend = array('nb_photos'=>'10','joueur'=>$dataGet->joueur,'token'=>$token,'id_serie'=>$dataGet->id_serie);

		$partie = Partie::createGame($dataSend);
	}

	static public function putStartGame() {
		$app = Slim::getInstance();

		$token = $app->request->get('token');

		$partie = Partie::startGame($token);
	}

	static public function putEndGame() {
		$app = Slim::getInstance();

		$dataGet = json_decode($app->request->getBody());		
		$token = $app->request->get('token');

		$partie = Partie::endGame($token,$dataGet);
	}

	static public function putPlayer($player) {
		$app = Slim::getInstance();

		$token = $app->request->get('token');

		$partie = Partie::findByToken($token);
		$partie->joueur = $player;

		$partie->save();
	}

}