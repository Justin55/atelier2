<?php 

namespace gestionnaire\api\controller;

use gestionnaire\common\model\Photos as Photos;
use gestionnaire\common\model\Serie as Serie;
use gestionnaire\api\view\Json as JSON;
/**
* 
*/
use Slim\Slim as Slim;

class photosController extends AbstractController
{

	static public function getPhotos() {
		$p = Photos::findAll();
		$v = JSON::TranslatePhotosToJson($p);
	}

	static public function getPhotosbyID($id) {
		$p = Photos::findOne($id);
		$v = JSON::TranslatePhotoToJson($p,$id);
	}

	static public function getPhotosBySerie($id) {
		$s = Serie::findOne($id);

		if (!isset($s)) {
			self::displayError('404',array("codeError" => "401","errorMessage" => "ressource serie/$id not found"));
		}
		else {
			$p = $s->photos();
			$v = JSON::TranslatePhotosBySeriesToJson($p);
		}
	}	
}