<?php 

namespace gestionnaire\api\controller;

use gestionnaire\common\model\Serie as Serie;
use gestionnaire\api\view\Json as JSON;
/**
* 
*/
use Slim\Slim as Slim;

class serieController extends AbstractController
{

	static public function getSeries() {
		$s = Serie::findAll();
		$v = JSON::TranslateSeriesToJson($s);
	}

	static public function getSeriebyID($id) {
		$s = Serie::findOne($id);
		if ( !is_null($s) ) {
			$v = JSON::TranslateSerieToJson($s,$id);
		}
		else {
			self::displayError('404',array("codeError" => "401","errorMessage" => "ressource serie/$id not found"));
		}
	}
}