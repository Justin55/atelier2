<?php 

namespace gestionnaire\admin\view;

use Slim\Slim as Slim;


class View{
	// OK
	public function displayIndex(){
		$app = Slim::getInstance();
		$app->render('index.twig', array('toto'=>'Page d\'accueil Admin'));
	}

	public function displayForm($tabl){
		$app = Slim::getInstance();
		$app->render('photo.twig', array('toto'=>'Ajouter une photo','villes'=>$tabl));	
	}

	public function displaySerie(){
		$app = Slim::getInstance();
		$app->render('serie.twig', array('toto'=>'Créer une série'));
	}

	// OK
	public function displayInscription(){
		$app = Slim::getInstance();
		$app->render('utilisateur.twig', array('toto'=>'S\'inscrire'));
	}

	public function displayConnexion(){
		$app = Slim::getInstance();
		$app->render('connexion.twig', array('toto'=>'Authentification'));
	}

	public function displayPhototoSerie(){
		$app = Slim::getInstance();
		$app->render('photoserie.twig', array('toto'=>'Ajout de photos dans une série'));
	}

	public function displayAuth(){
		$app = Slim::getInstance();
		$app->render('indexAuth.twig', array('toto'=>'Bienvenue'));
	}

	public function displayError(){
		$app = Slim::getInstance();
		$app->render('permission.twig', array('toto'=>'Opération non permise !'));
	}
}