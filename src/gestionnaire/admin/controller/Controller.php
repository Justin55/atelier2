<?php  

namespace gestionnaire\admin\controller;

use Slim\Slim as Slim;
use gestionnaire\admin\view\View as View;
use gestionnaire\common\model\Photos as Photos;
use gestionnaire\common\model\User as Users;
use gestionnaire\common\model\Serie as Serie;
use gestionnaire\admin\controller\photoController as pController;
use gestionnaire\admin\controller\serieController as sController;
use gestionnaire\admin\controller\utilController as uController;
use gestionnaire\admin\controller\connectController as cController;

class Controller{
	public static function dispatch($i){
		$v = new View();
		switch ($i){
			// OK
			case 'index':
			$v->displayIndex();
			break;

			case 'form':
			$s = Serie::findAll();
			$v->displayForm($s);
			break;

			case 'form_serie':
			$v->displaySerie();
			break;

			// OK
			case 'inscr':
			$v->displayInscription();
			break;

			case 'authentif':
			$v->displayConnexion();
			break;

			case 'form_addtoserie':
			$v->displayPhototoSerie();
			break;

			case 'indexA':
			$v->displayAuth();
			break;

			case 'error':
			$v->displayError();
			break;

			default:
			/*ecrit un truc*/
			break;

		}

	}
}