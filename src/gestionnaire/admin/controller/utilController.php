<?php 

namespace gestionnaire\admin\controller;
require_once("../../../utils/password.php");

use Slim\Slim as Slim;
use gestionnaire\admin\view\View as View;
use gestionnaire\common\model\User as User;
use gestionnaire\admin\controller\utilController as uController;
use gestionnaire\admin\controller\hash as Hash;

/**
* 
*/
class utilController
{
	
	static public function uUser()
	{
		$app = Slim::getInstance();

		$user = $app->request->post('user');
		$pass = $app->request->post('pass');

		$passwordClear = $pass ;
		$hash = password_hash($passwordClear,PASSWORD_BCRYPT,['cost' => 13]) ;

		$tabl = array('user'=>$user,'pass'=>$hash); 
		$send = User::postUser($tabl);

		echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
	<meta charset=\"UTF-8\">
	<title>Gestion photo</title>
	<link type=\"text/css\" rel=\"stylesheet\" href=\"../ressources/style/materialize.min.css\"  media=\"screen,projection\"/>
	<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
</head>
<body>
	<header>
		<nav class=\"top-nav\">
			<div class=\"container\">
				<div class=\"nav-wrapper\">
					<a class=\"page-title\">ATTENTION</a>
				</div>
			</div>
		</nav>
	</header>";

		echo 'L\'utilisateur <b>'.$user.'</b> a été crée.';
		echo "<br><a href='../'> Retour vers la page principale </a> <br>
		    <a href='index.php/inscr'> Retour vers le formulaire </a> <br>";
		echo "</body>
</html>";

	}

}