<?php 

namespace gestionnaire\admin\controller;
require_once("../../../utils/password.php");

use Slim\Slim as Slim;
use gestionnaire\admin\view\View as View;
use gestionnaire\common\model\User as User;
use gestionnaire\admin\controller\utilController as uController;

class connectController {

	static public function connect() {
		$app = Slim::getInstance();

		$user = $app->request->post('user');
		$pass = $app->request->post('pass');

		$u = User::findHash($user);

		// hash
		$password = password_verify($pass, $u->pass);

		if ($password==true) {
			session_start();
			$_SESSION['user'] = $user;
			$_SESSION['pass'] = $pass;

			return true;
		}
		else {
		 	return false;
		}
	}


	static public function deconnect() {
		session_start();
		session_destroy();
	}
}
