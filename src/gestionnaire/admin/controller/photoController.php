<?php 

namespace gestionnaire\admin\controller;

use Slim\Slim as Slim;
use gestionnaire\admin\view\View as View;
use gestionnaire\common\model\Photos as Photos;

class photoController
{
	static public function pPhoto()
	{
		$app = Slim::getInstance();

		
		echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
	<meta charset=\"UTF-8\">
	<title>Gestion photo</title>
	<link type=\"text/css\" rel=\"stylesheet\" href=\"../ressources/style/materialize.min.css\"  media=\"screen,projection\"/>
	<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
</head>
<body>
	<header>
		<nav class=\"top-nav\">
			<div class=\"container\">
				<div class=\"nav-wrapper\">
					<a class=\"page-title\">ATTENTION</a>
				</div>
			</div>
		</nav>
	</header>";
		
		//Vérification de la latitude et longitude		
		$lat = $app->request->post('latitude');
		$veriflat = preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $lat);
			
		$lng = $app->request->post('longitude');
		$veriflng = preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $lng);


		$target_dir = '../ressources/img/';
		$target_file = $target_dir . basename($_FILES["url"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Vérifie si l'image est une vrai image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["url"]["tmp_name"]);
		    if($check !== false) {
		        echo "Le fichier est une image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "Le fichier n'est pas une image.";
		        $uploadOk = 0;
		    }
		}
		// Vérifie si le fichier existe
		if (file_exists($target_file)) {
		    echo "Désolé, le fichier existe déjà. <br>";
		    $uploadOk = 0;
		}
		// Vérifie la taille 
		if ($_FILES["url"]["size"] > 50000000) {
		    echo "Désolé, votre fichier est trop volumineux. <br>";
		    $uploadOk = 0;
		}
		// Vérifie le format
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    echo "Désolé, seul les formats JPG, JPEG, PNG & GIF sont autorisés. <br>";
		    $uploadOk = 0;
		}
		// si $uploadOk = 0 alors erreur
		if ($uploadOk == 0) {
		    echo "Désolé, votre fichier n'a pas été chargé. <br>
		    <form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
			<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
			</form>
			<br>
		    <a href='form'> Retour vers le formulaire </a> <br>";
		// upload le fichier si tout ok
		} else if(($veriflat!=0)&&($veriflng!=0)){
		    if (move_uploaded_file($_FILES["url"]["tmp_name"], $target_file)) {
		        echo "Votre fichier ". basename( $_FILES["url"]["name"]). " a été chargé. <br>
		        <form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
				<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
				</form>
				<br>
		    	<a href='form'> Retour vers le formulaire </a> <br>";
		    } else {
		        echo "Désolé, il y a eu une ERREUR pendant le chargement de votre fichier. <br> ";
		    }
		}
	

			$nameimage = $_FILES["url"]["name"];

			$url = $app->request->post('url');
			// $lat = $app->request->post('latitude');
			// $lng = $app->request->post('longitude');
			$des = $app->request->post('description');
			$serie = $app->request->post('serie');

			$img = $nameimage.''.$url;

			$position = $lat.';'.$lng;

		if(($uploadOk != 0)&&($veriflat != 0)&&($veriflng !=0 )){

			$tabl = array('description'=>$des , 
				'position'=>$position , 
				'url'=>$img ,
				'id_serie'=>$serie);

			$send = Photos::postPhoto($tabl);
		} 
		else if(($uploadOk != 0)&&($veriflat == 0)&&($veriflng !=0 )){ 
			echo "\n La latitude n'est pas correcte.";
			echo "<br> <form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
				<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
				</form>
				<br>
		    	<a href='form'> Retour vers le formulaire </a> <br>";
		}
		else if(($uploadOk != 0)&&($veriflat != 0)&&($veriflng ==0 )){
			echo "\n La longitude n'est pas correcte.";
			echo "<br> <form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
				<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
				</form>
				<br>
		   		<a href='form'> Retour vers le formulaire </a> <br>";
		}
		else if(($uploadOk != 0)&&($veriflat == 0)&&($veriflng ==0 )){
			echo "\n La latitude et la longitude ne sont pas correctes.";
			echo "<br><form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
				<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
				</form>
				<br>
		    	<a href='form'> Retour vers le formulaire </a> <br>";
		}

		echo "</body>
</html>";
	}
}