<?php 

namespace gestionnaire\admin\controller;

use Slim\Slim as Slim;
use gestionnaire\admin\view\View as View;
use gestionnaire\common\model\Serie as Serie;
use gestionnaire\admin\serieController as sController;

/**
* 
*/
class serieController
{
	
	static public function sSerie()
	{
		$app = Slim::getInstance();

		echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
	<meta charset=\"UTF-8\">
	<title>Gestion photo</title>
	<link type=\"text/css\" rel=\"stylesheet\" href=\"../ressources/style/materialize.min.css\"  media=\"screen,projection\"/>
	<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
</head>
<body>
	<header>
		<nav class=\"top-nav\">
			<div class=\"container\">
				<div class=\"nav-wrapper\">
					<a class=\"page-title\">ATTENTION</a>
				</div>
			</div>
		</nav>
	</header>";

		$dist = 300;

		$lat = $app->request->post('latitude');
		$veriflat = preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $lat);
			
		$lng = $app->request->post('longitude');
		$veriflng = preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $lng);

		$zoom = $app->request->post('zoom');

		$map = $lat.';'.$lng.';'.$zoom;
		$vil = $app->request->post('ville');

		if(($veriflat != 0)&&($veriflng !=0 )){

			$tabl = array('ville'=>$vil , 
				'map_refs'=>$map , 
				'distance'=>$dist);

			$send = Serie::postSerie($tabl);

			echo "La série <b>".$vil. "</b> a été crée.";
			echo "<br><form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
				<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
				</form>
				<br>
		    	<a href='authentif'> Retour vers la page d'administration </a> <br>";
		} 
		else if(($veriflat == 0)&&($veriflng !=0 )){ 
			echo "\n La latitude n'est pas correcte.";
			echo "<br><form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
				<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
				</form>
				<br>
		    	<a href='authentif'> Retour vers la page d'administration </a> <br>";
		}
		else if(($veriflat != 0)&&($veriflng ==0 )){
			echo "\n La longitude n'est pas correcte.";
			echo "<br><form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
				<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
				</form>
				<br>
		   		<a href='authentif'> Retour vers la page d'administration </a> <br>";
		}
		else if(($veriflat == 0)&&($veriflng ==0 )){
			echo "\n La latitude et la longitude ne sont pas correctes.";
			echo "<br><form method=\"POST\" action=\"../admin\" enctype=\"multipart/form-data\">
				<button type=\"submit\" name=\"envoyer\" value=\"Retour\" class=\"btn\">Deconnexion</button>
				</form>
				<br>
		    	<a href='authentif'> Retour vers la page d'administration </a> <br>";
		}

		echo "</body>
</html>";
	}

}