<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Photo-Locate | Game</title>
	<!--jQuery-->
    <script type="text/javascript" src='../ressources/script/lib/jquery-2.1.4.min.js'></script>
	<!--Angular-->
    <script type="text/javascript" src='../ressources/script/lib/angular.min.js'></script>
    <!--App-->
    <script type="text/javascript" src="../ressources/script/app/app.js"></script>
	<!-- Game -->
    <script type="text/javascript" src="../ressources/script/app/game/game.js"></script>
	<script type="text/javascript" src="../ressources/script/app/game/game_controller.js"></script>
	<script type="text/javascript" src="../ressources/script/app/game/game_directive.js"></script>
	<!-- Image -->
	<script type="text/javascript" src="../ressources/script/app/image/image_controller.js"></script>
	<script type="text/javascript" src="../ressources/script/app/image/image_directive.js"></script>
	<!--Lealfet-->
	<link rel="stylesheet" href="../ressources/leaflet/leaflet.css" />
	<script src="../ressources/leaflet/leaflet.js"></script>
	<!-- CSS -->
	<link rel="stylesheet" href="../ressources/style/screen.css" />
	<link rel="stylesheet" href="../ressources/js/js.js" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body ng-app="photoLocate">

<header>
	<h1>PhotoLocate</h1>
</header>

<div ng-controller="GameController" class="display">
	<game />
</div>

	
</body>
</html>