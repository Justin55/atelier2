<?php

require_once '../vendor/autoload.php';
require_once("../utils/password.php");

use Slim\Slim as Slim;
use Slim\Views\Twig as Twig;
use utils\Connect;
use gestionnaire\admin\controller\Controller as AdminController;
use gestionnaire\admin\controller\photoController as pController;
use gestionnaire\admin\controller\serieController as sController;
use gestionnaire\admin\controller\utilController as uController;
use gestionnaire\admin\controller\connectController as cController;

Connect::EloConfigure('../../conf/config.ini');

$app = new Slim();
$app->view(new Twig());

$view = $app->view();
$view->setTemplatesDirectory('../ressources/templates');

// debug mode
$view->parserOptions = array(
	'debug' => true);
// 

// ok
$app->get('/', function () use($app) {
	$c = cController::deconnect();
	$c = AdminController::dispatch('index');
});

// OK
$app->get('/inscr', function() use($app){
	$c = AdminController::dispatch('inscr');
});


$app->post('/addUtil', function() use($app){
	$c = uController::uUser();
});

// ok
$app->get('/authentif', function() use($app){
	$c = AdminController::dispatch('authentif');
});

$app->get('/form', function () use($app) {
	session_start();
	
	if ( isset($_SESSION['user']) ) {
		$c = AdminController::dispatch('form');
	}
	else {
		$c = AdminController::dispatch('error');
	}
});

$app->post('/addPhoto', function() use($app) {

	$c = pController::pPhoto();
});

// ok
$app->get('/form_serie', function() use($app){
	session_start();

	if ( isset($_SESSION['user']) ) {
		$c = AdminController::dispatch('form_serie');
	}
	else {
		$c = AdminController::dispatch('error');
	}
	
});

$app->post('/addSerie', function() use($app) {
	$c = sController::sSerie();
});

$app->post('/indexA', function() use($app){
	
	$c = cController::connect();

	if ($c==true) {
		$c = AdminController::dispatch('indexA');
	}
	else {
		echo '<body onLoad="alert(\'Utilisateur inconnu ou mot de passe incorrect\')">';
		echo '<meta http-equiv="refresh" content="0;URL=authentif">';
	}
});

$app->notFound(function () use ($app) {
	echo '<body onLoad="alert(\'Page non trouvée  \')">';
	echo '<meta http-equiv="refresh" content="0;URL=../admin">';	
});

$app->run();



